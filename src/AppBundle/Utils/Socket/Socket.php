<?php

namespace AppBundle\Utils\Socket;
use AppBundle\Utils\Socket\Source\TestDataSocket;
use AppBundle\Utils\Socket\Source\TrueFxSocket;
use Symfony\Component\Validator\Constraints\True;

/**
 * Created by PhpStorm.
 * User: maurizio
 * Date: 05/10/17
 * Time: 12.09
 */

class Socket
{
    /**
     * Il socket legge i dati in tempo reale da truefx.
     */
    const SOCKET_REAL = 0;

    /**
     * Il socket legge i dati di test dallo storico di truefx.
     */
    const SOCKET_TEST_TRUEFX = 1;

    /**
     * Il socket legge i dati di test da quelli salvati dallo scanner in tempo reale.
     */
    const SOCKET_TEST_SCANNER = 2;

    /**
     * @var TestDataSocket|TrueFxSocket
     */
    private $dataSocket = null;

    /**
     * Socket constructor.
     *
     * @param int $socketType
     * @param array $symbols
     * @param string $trueUrl
     * @param string $trueUser
     * @param string $truePwd
     * @param string $test_suffix
     */
    public function __construct($socketType = self::SOCKET_REAL, $symbols = array(), $trueUrl = '', $trueUser = '', $truePwd = '',  $test_suffix = '')
    {
        $this->dataSocket = null;

        if ($socketType === self::SOCKET_REAL) {
            $this->dataSocket = new TrueFxSocket($symbols, $trueUrl, $trueUser, $truePwd);
        } else {
            if ($socketType === self::SOCKET_TEST_TRUEFX) {
                $this->dataSocket = new TestDataSocket($symbols, TestDataSocket::TEST_TRUEFX, $test_suffix);
            } else {
                $this->dataSocket = new TestDataSocket($symbols, TestDataSocket::TEST_SCANNER, $test_suffix);
            }
        }
    }

    /**
     * Connessione alla sorgente dati del socket.
     *
     * @return bool
     */
    public function connect() {
        return $this->dataSocket->connect();
    }

    /**
     * Recupero dei dati dalla sorgente dati del socket.
     *
     * @return bool|string
     */
    public function getCsv() {
        return $this->dataSocket->getCsv();
    }
}