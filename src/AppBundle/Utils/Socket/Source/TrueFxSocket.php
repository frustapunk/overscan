<?php

namespace AppBundle\Utils\Socket\Source;

use GuzzleHttp\Client as guzzleClient;
use GuzzleHttp\Exception\RequestException;

/**
 * Created by PhpStorm.
 * User: maurizio
 * Date: 16/08/17
 * Time: 1.03
 *
 * Utilizzo:
 *
 * $socket = new TestDataSocket($simboli, $sorgente, $suffisso);
 *
 * if ($socket->connect()) {
 *      while (true) {
 *          $dati = $socket->getCsv();
 *
 *          [....]
 *      }
 * }
 *
 */
class TrueFxSocket
{
    /**
     * L'indirizzo elaborato di connessione.
     *
     * @var string
     */
    private $trueFxUrl = '';

    /**
     * Url di connessione.
     *
     * @var string
     */
    private $param_url = '';

    /**
     * User di connessione.
     *
     * @var string
     */
    private $param_user = '';

    /**
     * Password di connessione.
     *
     * @var string
     */
    private $param_password = '';

    /**
     * I simboli da recuperare.
     *
     * @var array
     */
    private $simboli = array();

    /**
     * TrueFxConnection constructor.
     *
     * @param array $simboli
     * @param string $trueFx_url
     * @param string $trueFx_user
     * @param string $trueFx_password
     */
    public function __construct($simboli = array(), $trueFx_url = '', $trueFx_user = '', $trueFx_password = ''){
        $this->simboli = $simboli;
        $this->param_url = $trueFx_url;
        $this->param_user = $trueFx_user;
        $this->param_password = $trueFx_password;
    }

    /**
     * Crea la connessione al server TrueFx e crea l'url di lavoro.
     *
     * @return bool
     */
    public function connect() {
        $q = md5(time());

        $url = $this->param_url.'?'.'u='.$this->param_user.'&p='.$this->param_password.'&q='.$q;

        $sym_uri = '&c=';

        foreach ($this->simboli as $sym) {
            $sym_uri .= trim($sym).',';
        }

        $url .= substr($sym_uri, 0, strlen($sym_uri) - 1).'&f=csv&s=y';

        $client = new guzzleClient();

        $out = false;

        try {
            $response = $client->get($url, array());
            if ($response->getStatusCode() == 200 || $response->getStatusCode() == 201 || $response->getStatusCode() == 202 || $response->getStatusCode() == 204) {
                $this->trueFxUrl = $this->param_url.'?id='.trim(str_replace(array("\n", "\r"), array('', ''), $response->getBody()->getContents()));
                $out = true;
            }
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            $out = false;
        }

        return $out;
    }

    /**
     * Recupera i dati del forex da TrueFX.
     *
     * @return string|boolean
     */
    public function getCsv() {
        $out = false;

        $client = new guzzleClient();

        try {
            $response = $client->get($this->trueFxUrl, array());
            if ($response->getStatusCode() == 200 || $response->getStatusCode() == 201 || $response->getStatusCode() == 202 || $response->getStatusCode() == 204) {
                $out = $response->getBody()->getContents();
            }
        } catch (RequestException $e) {
            $out = false;
        }

        return $out;
    }
}