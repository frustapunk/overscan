<?php

namespace AppBundle\Utils\Socket\Source;

/**
 * Created by PhpStorm.
 * User: maurizio
 * Date: 16/08/17
 * Time: 1.03
 *
 * Utilizzo:
 *
 * $socket = new TestDataSocket($simboli, $sorgente, $suffisso);
 *
 * if ($socket->connect()) {
 *      while (true) {
 *          $dati = $socket->getCsv();
 *
 *          [....]
 *      }
 * }
 *
 */

class TestDataSocket
{
    /**
     * Indicano da dove leggere i dati di test.
     */
    const TEST_SCANNER = 0;
    const TEST_TRUEFX = 1;

    /**
     * Specifica da quele sorgente leggere i file.
     *
     * @var string
     */
    private $sorgente = self::TEST_TRUEFX;

    /**
     * Il suffisso per i file da leggere.
     *
     * @var string
     */
    private $suffix = '';

    /**
     * Il vettore contenente i fiel source.
     *
     * @var array
     */
    private $fileSources = array();

    /**
     * Il vettore con, simbolo per simbolo, l'utlimo orario letto.
     *
     * @var array
     */
    private $orariRighe = array();

    /**
     * Il vettore con, simbolo per simbolo, l'utlima riga letta.
     *
     * @var array
     */
    private $ultimaRiga = array();

     /**
     * TestDataSocket constructor.
      *
     * @param array $simboli
     * @param int $sorgente
     * @param string $suffisso
     */
    public function __construct($simboli, $sorgente, $suffisso ) {
        $this->sorgente = $sorgente;
        $this->suffix = $suffisso;

        $this->initFiles($simboli);
    }

    /**
     * Funzione fittizia per compatibilità con TrueFxSocket.
     *
     * @return bool
     */
    public function connect() {
        return true;
    }

    /**
     * Recupera i dati in csv.
     */
    public function getCsv() {
        $out = '';

        foreach ($this->fileSources as $simbolo => $fs) {
            $out .= $this->getSimboloRow($simbolo)."\n";
        }

        return $out;
    }

    /**
     * TestStream destructor.
     */
    public function __destruct() {
        $this->closeFiles();
    }

    /**
     * Inizializza i file e prepara gli orari di lettura dei singoli simboli.
     *
     * @param array $simboli
     */
    private function initFiles($simboli) {
        foreach ($simboli as $simbolo) {
            $this->orariRighe[$simbolo] = 0;
            $this->ultimaRiga[$simbolo] = '';

            $cartellaBase = '/home/clienti/frustapunk/binary-options/overscan/data_test/';

            if ($this->sorgente === self::TEST_TRUEFX) {
                $cartellaBase .= 'truefx/';
            }

            if ($this->sorgente === self::TEST_SCANNER) {
                $cartellaBase .= 'scanner/';
            }

            $nomeFile = $cartellaBase
                .str_replace('/', '', $simbolo).'-'
                .$this->suffix.'.csv';

            if (file_exists($nomeFile)) {
                $this->fileSources[$simbolo] = fopen($nomeFile, 'r');
            }
        }
    }

    /**
     * Chiude tutti i file aperti.
     */
    private function closeFiles() {
        foreach ($this->fileSources as $key => $fs) {
            if (isset($this->fileSources[$key])) {
                fclose($this->fileSources[$key]);
                unset($this->fileSources[$key]);
            }
        }
    }

    /**
     * Recupera la riga del singolo simbolo.
     *
     * @param $simbolo
     * @return bool|string
     */
    private function getSimboloRow($simbolo) {
        $out = false;

        if (($riga = fgetcsv($this->fileSources[$simbolo], 0, ','))) {
            $ore = (int)substr($riga[1], 9,2);
            $minuti = (int)substr($riga[1], 12,2);
            $secondi = (int)substr($riga[1], 15,2);
            $millisecondi = substr($riga[1], 18);
            $anno = (int)substr($riga[1], 0,4);
            $mese = (int)substr($riga[1], 4,2);
            $giorno = (int)substr($riga[1], 6,2);

            $orario = mktime($ore, $minuti, $secondi, $mese, $giorno, $anno);

            $bid = $riga[2];
            $ask = $riga[3];

            $out = $simbolo.','.trim($orario).str_pad(trim($millisecondi), 3, '0', STR_PAD_RIGHT).','.trim($bid).',0,'.trim($ask).',0,0,0,0';
        }

        return $out;
    }
}